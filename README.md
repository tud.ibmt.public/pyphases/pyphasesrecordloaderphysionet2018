# PhysioNet 2018 "You Snooze you Win" Recordloader - Plugin

pyPhases plugin that adds a Recordloader to your Project. The [RecordLoader](https://gitlab.com/tud.ibmt.public/pyphases/pyphasesrecordloader) plugin is required.
